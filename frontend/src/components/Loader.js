import React from 'react'
import Spinner from 'react-bootstrap/Spinner'

const Loader = ({ height = "50vh" }) => {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: height,
      }}
    >
      <Spinner
        animation="border"
        role="status"
        style={{
          display: 'block',
          width: '100px',
          height: '100px',
          margin: 'auto',
        }}
      >
        <span className="sr-only">Loading...</span>
      </Spinner>
    </div>
  )
}

export default Loader
