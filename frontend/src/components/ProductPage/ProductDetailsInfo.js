import React, { useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import Image from 'react-bootstrap/Image'
import ListGroup from 'react-bootstrap/ListGroup'
import Button from 'react-bootstrap/Button'

import QuantityMenu from '../QuantityMenu'
import Rating from '../Rating'

const ProductDetailsInfo = ({ product }) => {
  const { id } = useParams()
  const navigate = useNavigate()

  const [quantity, setQuantity] = useState(1)

  const changeQuantityHandler = (e) => setQuantity(e.target.value)

  const stockStatus = product.countInStock > 0 ? 'In Stock' : 'Out Of Stock'

  const addToCartHandler = () => {
    navigate(`/cart/${id}?qty=${quantity}`)
  }

  return (
    <Row>
      <Col md={6}>
        <Image src={product.image} alt={product.name} fluid />
      </Col>

      <Col md={3}>
        <ListGroup variant="flush">
          <ListGroup.Item>
            <h3>{product.name}</h3>
          </ListGroup.Item>

          <ListGroup.Item>
            <Rating rating={product.rating} reviews={product.numReviews} />
          </ListGroup.Item>

          <ListGroup.Item>Price:{product.price} &euro;</ListGroup.Item>
          <ListGroup.Item>Description:{product.description}</ListGroup.Item>
        </ListGroup>
      </Col>

      <Col md={3}>
        <Card>
          <ListGroup variant="flush">
            <ListGroup.Item>
              <Row>
                <Col>Price:</Col>
                <Col>{product.price}&euro;</Col>
              </Row>
            </ListGroup.Item>

            <ListGroup.Item>
              <Row>
                <Col>Status:</Col>
                <Col>{stockStatus}</Col>
              </Row>
            </ListGroup.Item>

            {product.countInStock > 0 && (
              <ListGroup.Item>
                <Row>
                  <Col>Quantity:</Col>
                  <Col>
                    <QuantityMenu
                      stock={product.countInStock}
                      value={quantity}
                      onChange={changeQuantityHandler}
                    />
                  </Col>
                </Row>
              </ListGroup.Item>
            )}

            <ListGroup.Item>
              <Button
                type="button"
                className="btn btn-block"
                onClick={addToCartHandler}
                disabled={product.countInStock === 0}
              >
                Add to Cart
              </Button>
            </ListGroup.Item>
          </ListGroup>
        </Card>
      </Col>
    </Row>
  )
}

export default ProductDetailsInfo
