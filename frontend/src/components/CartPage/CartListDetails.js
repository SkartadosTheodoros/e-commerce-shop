import React from 'react'
import { Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { addToCart, removeFromCart } from '../../actions/cart.actions'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'
import ListGroup from 'react-bootstrap/ListGroup'
import Button from 'react-bootstrap/Button'

import QuantityMenu from '../QuantityMenu'

const CartListDetails = ({ cart }) => {
  const dispatch = useDispatch()

  const removeFromCartHandler = (id) => {
    dispatch(removeFromCart(id))
  }

  return (
    <ListGroup variant="flush">
      {cart.map((cartItem) => (
        <ListGroup.Item key={cartItem.id}>
          <Row>
            <Col md={2}>
              {/* prettier-ignore */}
              <Image 
                src={cartItem.image} 
                alt={cartItem.name} 
                rounded 
                fluid 
              />
            </Col>

            <Col md={4}>
              <Link to={`/products/${cartItem.id}`}>{cartItem.name}</Link>
            </Col>

            <Col md={2}>{cartItem.price}</Col>

            <Col md={2}>
              {/* prettier-ignore */}
              <QuantityMenu
                stock={cartItem.countInStock}
                value={cartItem.quantity}
                onChange={(e) => dispatch(
                  addToCart(cartItem.id, Number(e.target.value))
                )}
              />
            </Col>

            <Col md={2}>
              <Button
                type="button"
                variant="light"
                onClick={removeFromCartHandler.bind(null, cartItem.id)}
              >
                <i className="fas fa-trash" />
              </Button>
            </Col>
          </Row>
        </ListGroup.Item>
      ))}
    </ListGroup>
  )
}

export default CartListDetails
