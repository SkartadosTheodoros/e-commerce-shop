import React from 'react'
import { useNavigate } from 'react-router-dom'

import Card from 'react-bootstrap/Card'
import ListGroup from 'react-bootstrap/ListGroup'
import Button from 'react-bootstrap/Button'

const CartSubtotalCard = ({ cart }) => {
  const navigate = useNavigate()

  const checkoutHandler = () => {
    navigate('/login?redirect=shipping')
  }

  // prettier-ignore
  const totalProducts = cart.reduce(
    (acc, item) => acc + item.quantity , 0
  )

  const totalProductsCost = cart
    .reduce((acc, item) => acc + item.quantity * item.price, 0)
    .toFixed(2)

  return (
    <Card>
      <ListGroup variant="flush">
        <ListGroup.Item>
          <h2>Subtotal ({totalProducts}) items</h2>
          <div>Total amount : {totalProductsCost} &euro;</div>
        </ListGroup.Item>

        <ListGroup.Item>
          <Button
            type="button"
            className="btn btn-block"
            disabled={cart.length === 0}
            onClick={checkoutHandler}
          >
            Procced To Checkout
          </Button>
        </ListGroup.Item>
      </ListGroup>
    </Card>
  )
}

export default CartSubtotalCard
