import React from 'react'

import Form from 'react-bootstrap/Form'

const QuantityMenu = ({ stock, value, onChange }) => {
  const quantitySequenceValues = [...Array(stock).keys()]

  return (
    <Form.Control as="select" value={value} onChange={onChange}>
      {quantitySequenceValues.map((element) => (
        <option key={element + 1} value={element + 1}>
          {element + 1}
        </option>
      ))}
    </Form.Control>
  )
}

export default QuantityMenu