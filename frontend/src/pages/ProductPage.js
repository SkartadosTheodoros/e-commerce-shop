import React, { Fragment, useEffect } from 'react'
import { Link, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { listProductDetails } from '../actions/product.actions'

import ProductDetailsInfo from '../components/ProductPage/ProductDetailsInfo'
import Loader from '../components/Loader'
import Message from '../components/Message'

const ProductPage = () => {
  const { id } = useParams()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(listProductDetails(id))
  }, [dispatch, id])

  const { loading, product, error } = useSelector(
    (state) => state.productDetails
  )

  return (
    <Fragment>
      <Link className="btn btn-light my-3" to="/">
        Go Back
      </Link>

      {loading ? (
        <Loader height="30vh" />
      ) : error ? (
        <Message variant="danger">{error}</Message>
      ) : (
        <ProductDetailsInfo product={product} />
      )}
    </Fragment>
  )
}

export default ProductPage
