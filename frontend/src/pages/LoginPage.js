import React, { useState, useEffect } from 'react'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { login } from '../actions/user.actions'

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import FormContainer from '../components/FormContainer'
import Loader from '../components/Loader'
import Message from '../components/Message'

const LoginPage = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const location = useLocation()

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const emailChangeHandler = (e) => setEmail(e.target.value)
  const passwordChangeHandler = (e) => setPassword(e.target.value)

  const redirect = location.search ? location.search.split('=')[1] : '/'
  const { loading, error, userInfo } = useSelector((state) => state.userLogin)

  useEffect(() => {
    if (userInfo) {
      navigate(redirect, { replace: true })
    }
  }, [navigate, redirect, userInfo])

  const submitHandler = (event) => {
    event.preventDefault()
    dispatch(login(email, password))
  }

  return (
    <FormContainer>
      <h1>Sign in</h1>

      {loading ? (
        <Loader />
      ) : (
        <>
          {error && <Message variant="danger">{error}</Message>}

          <Form onSubmit={submitHandler}>
            <Form.Group controlId="email" className="pb-3">
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={emailChangeHandler}
              />
            </Form.Group>

            <Form.Group controlId="password" className="pb-3">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter password"
                value={password}
                onChange={passwordChangeHandler}
              />
            </Form.Group>

            <Button type="submit" variant="primary">
              Sign in
            </Button>
          </Form>

          <Row className="py-3">
            <Col>
              New customer?{' '}
              <Link
                to={redirect ? `/register?redirect=${redirect}` : `/register`}
              >
                Register
              </Link>
            </Col>
          </Row>
        </>
      )}
    </FormContainer>
  )
}

export default LoginPage
