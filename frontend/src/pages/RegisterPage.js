import React, { useState, useEffect } from 'react'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { register } from '../actions/user.actions'

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import FormContainer from '../components/FormContainer'
import Loader from '../components/Loader'
import Message from '../components/Message'

const RegisterPage = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const location = useLocation()

  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [message, setMessage] = useState(null)

  const nameChangeHandler = (e) => setName(e.target.value)
  const emailChangeHandler = (e) => setEmail(e.target.value)
  const passwordChangeHandler = (e) => setPassword(e.target.value)
  const confirmPasswordChangeHandler = (e) => setConfirmPassword(e.target.value)

  const redirect = location.search ? location.search.split('=')[1] : '/'
  const { loading, error, userInfo } = useSelector(
    (state) => state.userRegister
  )

  useEffect(() => {
    if (userInfo) {
      navigate(redirect, { replace: true })
    }
  }, [navigate, redirect, userInfo])

  const submitHandler = (event) => {
    event.preventDefault()

    if (password !== confirmPassword) {
      setMessage('Password do not match')
    } else {
      dispatch(register(name, email, password))
    }
  }

  return (
    <FormContainer>
      <h1>Sign up</h1>

      {loading ? (
        <Loader />
      ) : (
        <>
          {error && <Message variant="danger">{error}</Message>}
          {message && <Message variant="danger">{message}</Message>}

          <Form onSubmit={submitHandler}>
            <Form.Group controlId="name" className="pb-3">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="name"
                placeholder="Enter name"
                value={name}
                onChange={nameChangeHandler}
              />
            </Form.Group>

            <Form.Group controlId="email" className="pb-3">
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={emailChangeHandler}
              />
            </Form.Group>

            <Form.Group controlId="password" className="pb-3 mt-4">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter password"
                value={password}
                onChange={passwordChangeHandler}
              />
            </Form.Group>

            <Form.Group controlId="confirmPassword" className="pb-3">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter password"
                value={confirmPassword}
                onChange={confirmPasswordChangeHandler}
              />
            </Form.Group>

            <Button type="submit" variant="primary">
              Register
            </Button>
          </Form>

          <Row className="py-3">
            <Col>
              Have an account?{' '}
              <Link to={redirect ? `/login?redirect=${redirect}` : `/login`}>
                Sign in
              </Link>
            </Col>
          </Row>
        </>
      )}
    </FormContainer>
  )
}

export default RegisterPage
