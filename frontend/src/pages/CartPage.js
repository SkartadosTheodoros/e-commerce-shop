import React, { useEffect } from 'react'
import { Link, useParams, useLocation } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { addToCart } from '../actions/cart.actions'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import CartListDetails from '../components/CartPage/CartListDetails'
import CartSubtotalCard from '../components/CartPage/CartSubtotalCard'
import Message from '../components/Message'

const CartPage = () => {
  const { id } = useParams()
  const { search } = useLocation()
  const dispatch = useDispatch()

  //prettier-ignore
  const quantity = search 
    ? Number(search.split('=')[1]) 
    : 1

  useEffect(() => {
    if (id) {
      dispatch(addToCart(id, quantity))
    }
  }, [dispatch, id, quantity])

  const cart = useSelector((state) => state.cart)

  return (
    <Row>
      <Col md={8}>
        <h1>Shopping Cart</h1>

        {cart.cartItems === 0 ? (
          <Message>
            Your cart is empty <Link to="/">Go back</Link>
          </Message>
        ) : (
          <CartListDetails cart={cart.cartItems} />
        )}
      </Col>

      <Col md={4}>
        <CartSubtotalCard cart={cart.cartItems} />
      </Col>
    </Row>
  )
}

export default CartPage
