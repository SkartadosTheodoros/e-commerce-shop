import React, { Fragment, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { listProducts } from '../actions/product.actions'

import Col from 'react-bootstrap/esm/Col'
import Row from 'react-bootstrap/esm/Row'

import Product from '../components/HomePage/Product'
import Loader from '../components/Loader'
import Message from '../components/Message'

const HomePage = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(listProducts())
  }, [dispatch])

  //prettier-ignore
  const { loading, products, error } = useSelector(
    (state) => state.productList
  )

  return (
    <Fragment>
      <h1>Latest Products</h1>

      {loading ? (
        <Loader />
      ) : error ? (
        <Message variant="danger">{error}</Message>
      ) : (
        <Row>
          {products.map((product) => (
            <Col key={product._id} sm={12} md={6} lg={4} xl={3}>
              <Product product={product} />
            </Col>
          ))}
        </Row>
      )}
    </Fragment>
  )
}

export default HomePage
