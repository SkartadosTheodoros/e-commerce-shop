export const getErrorMessage = (error) => {
  // If backend send error using throw new error
  if (error.response && error.response.data.message) {
    return error.response.data.message
  }

  // If backend send error as json data
  if (error.response && error.response.data) {
    return error.response.data
  }

  return error.message
}
