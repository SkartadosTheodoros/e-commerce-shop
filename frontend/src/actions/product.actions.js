import axios from 'axios'
import { getErrorMessage } from '../utils/errorMessage'

export const listProducts = () => async (dispatch) => {
  try {
    dispatch({
      type: 'PRODUCT_LIST_REQUEST',
    })

    const { data } = await axios.get('/api/products')

    dispatch({
      type: 'PRODUCT_LIST_SUCCESS',
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: 'PRODUCT_LIST_FAIL',
      payload: getErrorMessage(error),
    })
  }
}

export const listProductDetails = (id) => async (dispatch) => {
  try {
    dispatch({
      type: 'PRODUCT_DETAILS_REQUEST',
    })

    const { data } = await axios.get(`/api/products/${id}`)

    dispatch({
      type: 'PRODUCT_DETAILS_SUCCESS',
      payload: data,
    })
  } catch (error) {
    dispatch({
      type: 'PRODUCT_DETAILS_FAIL',
      payload: getErrorMessage(error),
    })
  }
}
