import axios from 'axios'
import { getErrorMessage } from '../utils/errorMessage'

export const login = (email, password) => async (dispatch) => {
  try {
    dispatch({
      type: 'USER_LOGIN_REQUEST',
    })

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    }

    const body = {
      email,
      password,
    }

    const { data } = await axios.post('/api/users/login', body, config)

    dispatch({
      type: 'USER_LOGIN_SUCCESS',
      payload: data,
    })

    localStorage.setItem('userInfo', JSON.stringify(data))
  } catch (error) {
    dispatch({
      type: 'USER_LOGIN_FAIL',
      payload: getErrorMessage(error),
    })
  }
}

export const logout = () => async (dispatch) => {
  dispatch({
    type: 'USER_LOGOUT',
  })

  localStorage.removeItem('userInfo')
}

export const register = (name, email, password) => async (dispatch) => {
  try {
    dispatch({
      type: 'USER_REGISTER_REQUEST',
    })

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    }

    const body = {
      name,
      email,
      password,
    }

    const { data } = await axios.post('/api/users/register', body, config)

    dispatch({
      type: 'USER_REGISTER_SUCCESS',
      payload: data,
    })

    dispatch({
      type: 'USER_LOGIN_SUCCESS',
      payload: data,
    })

    localStorage.setItem('userInfo', JSON.stringify(data))
  } catch (error) {
    dispatch({
      type: 'USER_REGISTER_FAIL',
      payload: getErrorMessage(error),
    })
  }
}
