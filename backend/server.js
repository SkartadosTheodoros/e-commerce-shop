import express from 'express'
import dotenv from 'dotenv'
import colors from 'colors'
import connectDB from './config/database.js'

import productRoutes from './routes/product.router.js'
import userRoutes from "./routes/user.router.js"

import { routeNotFound } from './middlewares/error.middleware.js'
import { errorHandler } from './middlewares/error.middleware.js'

dotenv.config()
connectDB()

const app = express()
app.use(express.json())

app.use('/api/products', productRoutes)
app.use('/api/users', userRoutes)

app.use(routeNotFound)
app.use(errorHandler)

const host = process.env.SERVER_HOST || 'localhost'
const port = process.env.SERVER_PORT || 5000
const mode = process.env.NODE_ENV || 'development'

app.listen(
  port,
  host,
  console.log(`Server running in ${mode} mode on ${host}:${port}`.yellow.bold)
)
