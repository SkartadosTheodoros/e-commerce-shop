import mongoose from 'mongoose'
import dotenv from 'dotenv'
import colors from 'colors'
import connectBD from '../config/database.js'
import User from '../models/user.model.js'
import Product from '../models/product.model.js'
import Order from '../models/order.model.js'

import users from './data/users.js'
import products from './data/products.js'

dotenv.config()
connectBD()

const importData = async () => {
  try {
    await User.deleteMany()
    await Product.deleteMany()
    await Order.deleteMany()

    const createdUsers = await User.insertMany(users)
    const adminUser = createdUsers[0]._id

    const updateProducts = products.map((product) => {
      return { ...product, user: adminUser }
    })

    await Product.insertMany(updateProducts)
    console.log('Data seeded...'.green.inverse)
    process.exit()
  } catch (error) {
    console.log(`${error.message}`.red.inverse)
    process.exit(1)
  }
}

const destroyData = async () => {
  try {
    await User.deleteMany()
    await Product.deleteMany()
    await Order.deleteMany()

    console.log('Data destroyed...'.red.inverse)
    process.exit()
  } catch (error) {
    console.log(`${error.message}`.red.inverse)
    process.exit(1)
  }
}

if (process.argv[2] === '-d') {
  destroyData()
} else {
  importData()
}
