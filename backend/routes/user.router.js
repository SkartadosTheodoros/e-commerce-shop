import express from 'express'
import { protect } from '../middlewares/auth.middleware.js'
import {
  loginUser,
  registerUser,
  getUserProfile,
} from '../controllers/user.controller.js'

const router = express.Router()

router.post('/login', loginUser)
router.post('/register', registerUser)
router.get('/profile', protect, getUserProfile)

export default router
