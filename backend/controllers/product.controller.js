import asyncHandler from 'express-async-handler'
import Product from '../models/product.model.js'

//@Route GET /api/products
export const getProducts = asyncHandler(async (req, res) => {
  const products = await Product.find({})
  res.status(200).json(products)
})

//@Route GET /api/products/:id
export const getProductById = asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id)

  if (product) {
    res.status(200).json(product)
  } else {
    // 1st way
    // res.status(404).json({ message: 'Product not found' })

    // 2nd way
    res.status(404)
    throw new Error('Product not found')
  }
})
